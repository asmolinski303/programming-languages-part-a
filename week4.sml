fun n_times(f, n, x) =
    if n = 0
    then x
    else f (n_times(f, n-1, x))


fun increment x = x+1
fun double x = x+x

val x = n_times(double, 8, 4)
val y = n_times(increment, 7, 4)


fun triple_n_times(n, x) =
    n_times((fn x => 3*x), n, x)

fun map(f, xs) = 
    case xs of
         [] => []
       | x::xs' => (f x) :: (map(f, xs'))

fun filter(f, xs) = 
    case xs of
         [] => []
       | x::xs' => if f x then x::(filter(f, xs'))
                   else filter(f, xs')

val l = [1,2,3,4,5]
val li = map(increment, l)


fun double_or_triple f =
    if f 7
    then fn x => 2*x 
    else fn x => 3*x 


val doubble = double_or_triple(fn x => x-3 = 4)
val nine = (double_or_triple(fn x => x = 4)) 3

datatype exp = Constant of int
             | Negate of exp
             | Add of exp * exp
             | Multiply of exp * exp

fun true_of_all_constants(f, e) = 
    case e of 
         Constant i => f i
       | Negate e1 => true_of_all_constants(f, e1)
       | Add (e1, e2) => true_of_all_constants(f, e1) andalso
           true_of_all_constants(f, e2)
       | Multiply (e1, e2) => true_of_all_constants(f, e1) andalso
           true_of_all_constants(f, e2)

fun all_even e = true_of_all_constants(fn x => x mod 2 = 0, e)


val x = 1
fun f y = 
    let val x = y+1
    in
        fn z => x + y + z
    end

val g = f 4

val c = g 6

fun f g = 
let val x = 1
in g 2
end

val x = 4
fun h y = x+y
val z = f h

(* composition *)

fun compose (f, g) = fn x => f(g x)
    (*('b -> 'c) * ('a * 'b) -> ('a -> 'c) *)(* eqal to (f o g)x*)

fun sqrt_of_abs i = Math.sqrt(Real.fromInt(abs i))
fun sqrt_of_abs i = (Math.sqrt o Real.fromInt o abs) i

infix !>
(* functions of functions *)
fun x !> f = f x 
fun sqrt i = i !> abs !> Real.fromInt !> Math.sqrt

fun backup1 (f, g) = fn x => case f x of
                                  NONE => g x
                                | SOME y => y

fun backup2 (f, g) = fn x => f x handle _ => g x 

(* currying *)

val sorted3 = fn x => fn y => fn z => z >=y andalso y >= x

val t1 = sorted3 3 4 5
fun best_sorted x y z = y >= x andalso z >= y (*syntactic sugar for line 99*)

val t2 = best_sorted 10 11 12

(* folding *)
(* partial application *)
fun exists predicate xs = 
    case xs of
         [] => false
       | x::xs' => predicate x orelse exists predicate xs'


val list1 = [1,2,3,4,5,6,7,0]
(*val t3 = contains_zero list1*)

val pair_with_one : int list -> (int * int) list = List.map (fn x => (x, 1))
val list2 = pair_with_one list1

fun curry f x y = f(x, y)
fun uncurry f(x, y) = f x y
fun range(i, j) = if i > j then [] else i::range(i+1, j)
val countup = curry range 1
val to7 = countup 7


