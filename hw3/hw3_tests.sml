val test1 = only_capitals ["Adam","adam","adam","Ola","Adam"] = ["Adam","Ola","Adam"]
val test2 = only_capitals [] = []
val test3 = only_capitals ["Adam","adam","adam","ola","adam"] = ["Adam"]
val test4 = only_capitals ["Adam","adam","adam","Ola","Adam"] = ["Adam","Ola","Adam"]
val test5 = only_capitals ["A","B","C","o","d"] = ["A","B","C"]

val test6 = longest_string1 [] = ""
val test7 = longest_string1 ["Adama","Kitel","byl","w","deche"] = "Adama"
val test8 = longest_string1 ["Adamaa","Kitel","byl","w","deche"] = "Adamaa"
val test9 = longest_string1 ["Adama","Kitel","byl","w","strzalemwdziesiatke"] = "strzalemwdziesiatke"

val test10 = longest_string2 ["Adama","Kit","byl","w","deche"] = "deche"
val test11 = longest_string2 [] = ""
