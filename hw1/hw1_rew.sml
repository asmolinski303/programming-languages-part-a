(* functions from hw1 rewritten with higher order functions *)
fun is_older (d1 : int * int * int , d2: int * int * int)= 
    (#1 d1 < #1 d2) 
    orelse (#1 d1 = #1 d1 andalso #2 d2 < #2 d2)
    orelse (#1 d1 = #1 d1 andalso #2 d2 = #2 d2 andalso #3 d1 < #3 d2)

(*2*)
fun number_in_month (dates : (int * int * int) list , month) =
    List.foldl (fn (d, acc) => if (#2 d) = month then 1+acc else acc) 0 dates

(*3*)
fun number_in_months (dates : (int * int * int) list, months : int list)=
    List.foldl (fn (m, acc) => acc + number_in_month(dates, m)) 0 months

(*4*)
fun dates_in_month (dates : (int * int * int) list, month : int)=
    List.filter (fn d => (#2 d) = month) dates

(*5*)
fun dates_in_months(dates : (int * int * int) list, months : int list)=
    List.foldl(fn (m, acc) => acc @ dates_in_month(dates, m)) [] months 

(*6*)
fun get_nth (strings, nth)=
    List.nth(strings, nth) 

(*7*)
fun date_to_string (date : int * int * int)=
    let val months = ["January", "February", "March", "April", "May", "June","July","September", "August", "October","November", "December"]
        val month = get_nth(months, #2 date)
    in
        month ^ " " ^ Int.toString(#3 date) ^ ", " ^ Int.toString(#1 date)
    end

(*8*)
fun number_before_reaching_sum (sum : int, xs : int list)= 
    if sum < 1
    then 0
    else 1 + number_before_reaching_sum(sum - (hd xs), tl xs)
    List.foldl (fn (x, acc) => if sum < 1 then acc else 1+acc ) 0 xs

(*9*)
fun what_month (day: int)=
    let val days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    in
        number_before_reaching_sum(day, days)
    end

(*10*)
fun month_range (day1 : int, day2 : int)=
    if day2 < day1
    then []
    else what_month(day1) :: month_range(day1 + 1, day2)  

					   
(*11*)
fun oldest (dates : (int * int * int) list)=
    if null dates
    then NONE
    else let fun oldest_nonempty (dates : (int * int * int) list)=
        if null (tl dates) 
        then hd dates
        else let val tl_ans = oldest_nonempty(tl dates)
             in
                 if is_older(hd dates, tl_ans)
                 then hd dates
                 else tl_ans
             end
         in
             SOME (oldest_nonempty dates)
         end

	
    
			   
	     
			  
				 
	    
