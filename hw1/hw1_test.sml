(*is older test*)
val test1 = is_older((2023, 10, 11), (2023, 10, 12)) = true
val test2 = is_older((2023, 10, 12), (2023, 10, 11)) = false
val test3 = is_older((2023, 9, 11), (2023, 10, 12)) = true
val test4 = is_older((1999, 10, 11), (2023, 10, 12)) = true
val test5 = is_older((2023, 1, 1), (2023, 1, 1)) = false
val test6 = is_older((2023, 1, 1), (2023, 1, 2)) = true
val test7 = is_older((2023, 10, 2), (2023, 10, 1)) = false
val test8 = is_older((2023, 2, 1), (2023, 12, 31)) = true
val test9 = is_older((2023, 2, 28), (2023, 3, 31)) = true
(*number in month test*)
val d1 = (2022, 10, 12)
val d2 = (2022, 10, 12)
val d3 = (2022, 10, 12)
val d4 = (2022, 8, 12)
val d5 = (2022, 9, 12)
val d6 = (2022, 12, 12)
	     
val list_dates = [d1, d2, d3, d4, d5, d6]

val test10 = number_in_month(list_dates, 10) = 3
val test11 = number_in_month(list_dates, 8) = 1
val test12 = number_in_month(list_dates, 9) = 1
val test13 = number_in_month(list_dates, 12) = 1
val test14 = number_in_month(list_dates, 7) = 0
val test15 = number_in_month([], 7) = 0

(*number in months test*)

val months = [10, 9, 8, 12, 1]
val test16 = number_in_months(list_dates, months) = 6
val test17 = number_in_months(list_dates, [8]) = 1
val test18 = number_in_months(list_dates, [10]) = 3
val test19 = number_in_months(list_dates, [10, 9]) = 4
val test20 = number_in_months(list_dates, [7]) = 0
val test21 = number_in_months([], [7]) = 0

val test49 = dates_in_month(list_dates, 10) = [(2022, 10, 12), (2022, 10,12), (2022, 10, 12)]
val test50 = dates_in_month(list_dates, 9) = [(2022, 9, 12)]
val test51 = dates_in_month(list_dates, 8) = [(2022, 8, 12)]
val test52 = dates_in_month([], 10) = []

val test53 = dates_in_months(list_dates, [10]) = [(2022, 10, 12), (2022, 10,12), (2022, 10, 12)]
val test54 = dates_in_months(list_dates, [10, 9]) = [(2022, 10, 12), (2022, 10, 12), (2022, 10, 12), (2022, 9, 12)]
val test55 = dates_in_months(list_dates, [9]) = [(2022, 9, 12)]
val test56 = dates_in_months([], [10]) = []
val test56 = dates_in_months(list_dates, []) = []

val test23 = get_nth(["1", "2", "3", "4", "5", "7", "8", "9"], 1) = "1"
val test24 = get_nth(["1", "2", "3", "4", "5", "7", "8", "9"], 2) = "2"
val test25 = get_nth(["1", "2", "3", "4", "5", "7", "8", "9"], 3) = "3"
val test26 = get_nth(["1", "2", "3", "4", "5", "7", "8", "9"], 5) = "5"
val test27 = get_nth(["1", "2", "3", "4", "5", "7", "8", "9"], 4) = "4"
val test28 = get_nth(["1", "2", "3", "4", "5", "7", "8", "9"], 8) = "9"	 

val test29 = number_before_reaching_sum(1, [1,2,3,4,5,6,7,8,9,10]) = 1
val test30 = number_before_reaching_sum(2, [1,2,3,4,5,6,7,8,9,10]) = 2
val test31 = number_before_reaching_sum(3, [1,2,3,4,5,6,7,8,9,10]) = 2
val test32 = number_before_reaching_sum(4, [1,2,3,4,5,6,7,8,9,10]) = 3
val test33 = number_before_reaching_sum(10, [1,2,3,4,5,6,7,8,9,10]) = 4
val test34 = number_before_reaching_sum(11, [1,2,3,4,5,6,7,8,9,10]) = 5
val test35 = number_before_reaching_sum(15, [1,2,3,4,5,6,7,8,9,10]) = 5
val test36 = number_before_reaching_sum(16, [1,2,3,4,5,6,7,8,9,10]) = 6
							
val test37 = what_month(1) = 1
val test38 = what_month(32) = 2
val test39 = what_month(59) = 2
val test40 = what_month(62) = 3
val test41 = what_month(120) = 4
val test42 = what_month(121) = 5
						   
val test43 = month_range(1, 1) = [1]
val test44 = month_range(29, 32) = [1,1,1,2]
val test45 = month_range(59, 63) = [2,3,3,3,3]
val test46 = month_range(62, 62) = [3]
val test47 = month_range(120, 125) = [4,5,5,5,5,5]
val test48 = month_range(151, 155) = [5,6,6,6,6]

val dates = [(2023, 10, 10),(2023,10,9),(2022,10,14),(2024, 9, 1),(2022, 1, 8),(2020, 1, 1),(2023, 11,2)]
val dates2 = [(2023, 1, 1)]
val dates3 = []

val test49 = valOf(oldest(dates)) = (2020,1,1)
val test50 = valOf(oldest(dates2)) = (2023,1,1)
val test51 = oldest(dates3) = NONE

						
