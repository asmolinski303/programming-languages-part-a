(*your own types*)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank
datatype exp = Constant of int
             | Negate of exp
             | Add of exp * exp
             | Multiply of exp * exp

fun max_constant e =
    let fun max_of_two (e1, e2) = 
        let val m1 = max_constant(e1) 
            val m2 = max_constant(e2)
        in if m1 > m2 then m1 else m2 end
    in
    case e of 
         Constant i => i
       | Negate e2  => max_constant(e2)
       | Add (e1, e2)   => max_of_two(e1, e2)
       | Multiply (e1, e2)  => max_of_two(e1, e2)
    end

val test_exp = Add(Add(Constant 19, Negate (Constant 23)), Multiply(Constant 100, Constant(50)))
val max = max_constant test_exp

fun eval e =
    case e of
         Constant i => i
       | Negate e2 => ~(eval e2)
       | Add (e1, e2) => (eval e1) + (eval e2)
       | Multiply (e2, e1) => (eval e1) * (eval e2)


(* Lists using pattern matching *)

fun sum_list xs = 
    case xs of
         [] => 0 
       | x::xs' => x + sum_list xs'

fun append_list (xs, ys) = 
    case xs of
         [] => ys
       | x::xs' => x :: append_list(xs' , ys)

val sum_twone = sum_list([1,2,3,4,5,6])
val appended = append_list([1,2,3,4,5], [9])
