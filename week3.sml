(* poly_types *)
fun append (xs, ys) =
     case xs of
        [] => ys
       |x::xs' => x :: append(xs', ys) 


val l1 = ["Hi ", ", ", "I'"]
val l2 = ["am", " Adam ", ", sir."]

val l3 = append (l1, l2)

val l4 = [1,2,3,4]
val l5 = [5,6,7,8]
val l6 = append(l4, l5)


(* nested patterns *)

exception ListLengthMismatch
fun zip3 list_triple = 
    case list_triple of 
         ([], [], []) => []
       | (hd1::tl1, hd2::tl2, hd3::tl3) => (hd1, hd2, hd3)::zip3(tl1, tl2, tl3)
       | _ => raise ListLengthMismatch

fun unzip3 lst = 
    case lst of 
         [] => ([], [], [])
       | (a, b, c)::tl => let val (l1, l2, l3) = unzip3 tl
                          in
                              (a::l1, b::l2, c::l3)
                          end


val t1 = ([1, 2, 3], [4, 5, 6], [7, 8, 9])
val t2 = (["ha", "ha"],["hi", "hi"],["ho", "ho"])

val res1 = zip3(t1)
val res2 = zip3(t2)

val res3 = unzip3(res1)
val res4 = unzip3(res2)


fun nondecreasing xs = 
    case xs of 
         [] => true
       | x::xs' => case xs' of 
                        [] => true 
                      | y::ys' => x <= y andalso nondecreasing xs'

fun better_nondecreasing xs =
    case xs of
         [] => true
       | x::[] => true
       | x1::(x2::xs') => x1 <= x2 andalso better_nondecreasing (x2::xs')


(* excepition - declaration *)

exception MyException

fun maxlist (xs, ex) = 
    case xs of
         [] => raise ex
       | x::[] => x
       | x::xs' => Int.max(x, maxlist(xs', ex))

val max = maxlist([1,2,3,4], MyException) handle MyException => 42
val max2 = maxlist([], MyException) handle MyException => 42

(* Tail recursion *)

fun bad_rev xs =
    case xs of
         [] => []
       | x::xs' => (bad_rev xs') @ [x]

fun good_rev xs =
    let fun aux (xs, acc) =
        case xs of
             [] => acc
           | x::xs' => aux(xs', x::acc)
        in
            aux(xs, [])
        end

val x = [1,2,3,4,5,6,7,8,9,0,0,12,324,5,645,7]
val revx = good_rev x
