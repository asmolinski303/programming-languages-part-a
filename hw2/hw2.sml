(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)
                        
(* a *)       
fun all_except_option(my_string, strings) = 
    let fun is_in_list(strings) = 
    case strings of 
         [] => false
       | s::strings' => if same_string(my_string, s)
                        then true
                        else is_in_list(strings')
    in
        case is_in_list strings of true =>
            let fun inner(my_string, strings) =
                case strings of
                [] => []
              | s::strings' => case same_string(my_string, s) of
                                   true => inner(my_string, strings')
                                 | false => s :: inner(my_string, strings')
            in
                SOME (inner(my_string, strings))
            end
        | false => NONE
    end

(* b *)
 
fun get_substitutions1(substitutions, my_string) =
    case substitutions of
         [] => []
       | s::substitutions' => 
            let val aeo = all_except_option(my_string, s)
            in 
                case aeo of 
                    SOME aeo => aeo @ get_substitutions1(substitutions', my_string)
                  | NONE => get_substitutions1(substitutions', my_string)
            end
(* c *)       

fun get_substitutions2(substitutions, my_string) =
    let fun aux(substitutions, acc) = 
        case substitutions of
             [] => acc
           | s::substitutions' => 
                let val aeo = all_except_option(my_string, s)
                in 
                    case aeo of 
                        SOME aeo => aeo @ aux(substitutions', acc)
                      | NONE => aux(substitutions', acc) 
                end
    in
        aux(substitutions, [])
    end

(* d *)       
fun similar_names(substitutions, {first=f, last=l, middle=m}) = 
    let val all_names = f :: get_substitutions1(substitutions, f)
    in
        let fun produce all_names =
            case all_names of
                 [] => []
               | name::all_names' => 
                   {first=name, last=l, middle=m} :: produce(all_names')
                    
         in
             produce all_names
        end
    end
(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black

exception IllegalMove

(* put your solutions for problem 2 here *)
fun card_color (c, _) = 
    case  c of
        (Clubs | Spades) => Black
      | (Hearts| Diamonds) => Red

fun card_value (_, c) = 
    case c of
         Ace => 11
       | Num i => i
       | _ => 10

fun remove_card(cs, c, e) = 
    case cs of 
         [] => raise e
       | rc::cs' => case rc = c of
                         true => cs'
                       | false => rc::remove_card(cs', c, e)

fun all_same_color cs =
    case cs of
         [] => true
       | _::[] => true 
       | c1::(c2::cs') => (
            card_color c1 = card_color c2 andalso all_same_color(c2::cs')
        )

fun sum_cards cs =
    let fun sum(cs, acc) =
        case cs of
             [] => acc
           | c::cs' => sum(cs', card_value(c) + acc) 
    in
        sum(cs, 0)
    end

datatype move = Draw | Discard of card

fun officiate(cards, moves, goal) = 
    let fun loop(cards, moves, hand) =
        case sum_cards(hand) < goal of
             true => ( 
                case moves of 
                [] => hand
              | m::moves' => case m of
                             Draw => (case cards of
                                 [] => hand
                               | c::cs' => loop(cs', moves', c::hand))
                           | Discard c => 
                                 loop(cards, moves', remove_card(hand, c, IllegalMove))
                ) 
           | false => hand
    in
        let val hand = loop(cards, moves, [])
            val sum = sum_cards hand
            val prelim = case sum > goal of
                              true => (3 * (sum - goal)) 
                            | false => (goal - sum)
        in
            case all_same_color hand of
                 true => prelim div 2
               | false => prelim
        end
    end
