

(*val test5 = is_on_list("Adam", ["Adam","Kamil","Iwona","Ola","Kot"]) = true
val test6 = is_on_list("Iwona", ["Adam","Kamil","Iwona","Ola","Kot"]) =  true
val test7 = is_on_list("Ola", ["Adam","Kamil","Iwona","Ola","Kot"]) = true
val test8 = is_on_list("Kamil", ["Adam","Kamil","Iwona","Ola","Kot"]) = true
val test9 = is_on_list("Kamil", []) = false
val test10 = is_on_list("Kamil", ["Adam","Iwona","Ola","Kot"]) = false
*)


val test1 = valOf(all_except_option("Adam", ["Adam","Kamil","Iwona","Ola","Kot"]))= ["Kamil", "Iwona", "Ola","Kot"]
val test2 = valOf(all_except_option("Iwona", ["Adam","Kamil","Iwona","Ola","Kot"]))= ["Adam", "Kamil", "Ola","Kot"]
val test3 = valOf(all_except_option("Ola", ["Adam","Kamil","Iwona","Ola","Kot"]))= ["Adam", "Kamil", "Iwona", "Kot"]
val test4 = valOf(all_except_option("a", ["a","b","c","d","e"]))= ["b", "c","d","e"]
val test5 = valOf(all_except_option("b", ["a","b","c","d","e"]))= ["a", "c","d","e"]
val test6 = all_except_option("f", ["a","b","c","d","e"])= NONE
val test7 = all_except_option("x", ["a","b","c","d","e"])= NONE


val subs1 = [["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]]
val subs2 = [["adam","adamus"],["adam","sadam"],["kamil","kamilex","ola"]]

val test8 = get_substitutions1(subs1, "Jeff") = ["Jeffrey","Geoff","Jeffrey"]
val test9 = get_substitutions1(subs1, "Fred") = ["Fredrick"]
val test10 = get_substitutions1(subs2, "adam") = ["adamus","sadam"]

val test11 = get_substitutions2(subs1, "Jeff") = ["Jeffrey","Geoff","Jeffrey"]
val test12 = get_substitutions2(subs1, "Fred") = ["Fredrick"]
val test13 = get_substitutions2(subs2, "adam") = ["adamus","sadam"]

val test14 = card_color((Diamonds, Ace)) = Red 
val test15 = card_color((Hearts, Ace)) = Red 
val test16 = card_color((Clubs, Ace)) = Black 
val test17 = card_color((Spades, King)) = Black 

val test18 = card_value((Diamonds, Ace)) = 11 
val test19 = card_value((Diamonds, King)) = 10 
val test20 = card_value((Diamonds, Queen)) = 10 
val test21 = card_value((Diamonds, Jack)) = 10
val test22 = card_value((Diamonds, Num 2)) = 2 
val test23 = card_value((Diamonds, Num 3)) = 3 
val test24 = card_value((Diamonds, Num 4)) = 4 
val test25 = card_value((Diamonds, Num 5)) = 5 
val test26 = card_value((Diamonds, Num 6)) = 6 
val test27 = card_value((Diamonds, Num 7)) = 7 
val test28 = card_value((Diamonds, Num 8)) = 8 
val test29 = card_value((Diamonds, Num 9)) = 9 
val test30 = card_value((Diamonds, Num 100)) = 100


val deck1 = [(Spades, Ace),(Spades, Queen),(Spades, Jack),(Spades, Jack),(Spades,Num 2)]
val deck2 = [(Hearts, Ace),(Spades, Queen),(Spades, Jack),(Spades, Num 9),(Spades,Num 2)]
val deck3 = [(Spades, Ace),(Spades, Queen),(Hearts, Jack),(Spades, Num 9),(Spades,Num 2)]
val short_deck = [(Clubs, Jack)]
val empty_deck = []

val test36 = remove_card(deck1, (Spades, Jack), IllegalMove) = 
[(Spades, Ace), (Spades, Queen), (Spades, Jack), (Spades, Num 2)]

(*val test38 = remove_card(short_deck, (Spades, Ace), IllegalMove) (* exception *) 
val test39 = remove_card(empty_deck, (Spades, Ace), IllegalMove) (* exception *)
*)
val test40 = remove_card(short_deck, (Clubs, Jack), IllegalMove) = []

val test41 = remove_card(deck2, (Hearts, Ace), IllegalMove) = 
[(Spades, Queen), (Spades, Jack), (Spades, Num 9), (Spades, Num 2)] (* first elem *)

val test42 = remove_card(deck2, (Spades, Num 2), IllegalMove) = 
[(Hearts, Ace), (Spades, Queen), (Spades, Jack), (Spades, Num 9)] (* last elem *)

val test31 = all_same_color(deck1) = true
val test32 = all_same_color(deck2) = false
val test33 = all_same_color(deck3) = false
val test34 = all_same_color(empty_deck) = true
val test35 = all_same_color(short_deck) = true

val test43 = sum_cards deck1 = 11+10+10+10+2 
val test44 = sum_cards deck2 = 11+10+10+9+2 
val test45 = sum_cards deck3 = 11+10+10+9+2 
val test46 = sum_cards short_deck = 10 
val test47 = sum_cards empty_deck = 0 

val move_set1 = [Draw, Draw, Draw, Discard (Spades, Queen)]
val test48 = officiate(deck1, move_set1, 21) = 21






